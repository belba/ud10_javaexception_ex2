package excepciones;

public class CapturaExceptions extends Exception{

	
	private int codigoException;

	public CapturaExceptions(int codigoException) {
		super();
		this.codigoException = codigoException;
	}

	public CapturaExceptions() {
		super();
	}
	
	public String getMessage() {
		String mensaje = "";
		
		switch (codigoException) {
		case 190:
			mensaje = "Es par"; 
			break;
		case 191:
			mensaje = "Es impar"; 
			break;
		default:
			break;
		}
		return mensaje;
	}
	
	

}
