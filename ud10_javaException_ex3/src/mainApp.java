import excepciones.CapturaExceptions;

public class mainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final int numeroMax = 999;
		//Generamos un numero aleatorio
		int valoresGenerados = 0;
		
		try {
			System.out.println("Generando n�mero aleatorio.....");
			valoresGenerados = (int) Math.floor(Math.random()*numeroMax);
			System.out.println("El n�mero generado es: "+valoresGenerados);
			
			if(valoresGenerados %2==0) {
				  throw new CapturaExceptions(190);	
			}else {
				  throw new CapturaExceptions(191);	
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
