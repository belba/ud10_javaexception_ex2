import excepciones.CapturaExceptions;

public class mainApp {

	public static void main(String[] args) throws Exception {
		
		try {
			
			int bucle = 0;
			
			while (bucle != 5) {
				System.out.println("Mensaje mostrado por pantalla");
				bucle++;
			}
			if(bucle==5)
				throw new CapturaExceptions(190);				
			
		} catch (Exception e) {			
			System.out.println(e.getMessage());
		}finally{
			System.out.println("Programa terminado");
        }

	}

}
