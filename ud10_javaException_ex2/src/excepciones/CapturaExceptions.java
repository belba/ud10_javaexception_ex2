package excepciones;

public class CapturaExceptions extends Exception{

	
	private int codigoException;

	public CapturaExceptions(int codigoException) {
		super();
		this.codigoException = codigoException;
	}

	public CapturaExceptions() {
		super();
	}
	
	public String getMessage() {
		String mensaje = "";
		
		switch (codigoException) {
		case 190:
			mensaje = "Excepcion capturada con mensaje: Esto es un objeto Exception"; 
			break;
		default:
			break;
		}
		return mensaje;
	}
	
	

}
